require "fileutils"
require "time"
require "zip"
require 'optparse'

$file_pair = []

def ensure_directories
  FileUtils.mkdir_p(%w[temp/OEBPS temp/META-INF])
end

def create_mimetype_and_container_files
  File.write("temp/mimetype", "application/epub+zip")

  container_content = <<~XML
    <?xml version="1.0" encoding="UTF-8"?>
    <container version="1.0" xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
      <rootfiles>
        <rootfile full-path="OEBPS/content.opf" media-type="application/oebps-package+xml"/>
      </rootfiles>
    </container>

  XML
  File.write("temp/META-INF/container.xml", container_content)
end

def process_novel(file_path, part_regex, chapter_regex, prelog_regex)
  file_content = File.read(file_path)

  part_counter    = 0
  chapter_counter = 0
  buffer          = ""
  output_dir      = "temp/OEBPS"

  file_content.each_line do |line|
    # 去掉首尾空白字符
    line = line.strip

    # 跳过空行
    next if line.empty?

    if prelog_regex.match?(line)
      buffer = write_chapter_and_reset_buffer(buffer, part_counter, chapter_counter, output_dir)
      part_counter = chapter_counter = 0
      buffer << generate_html_structure(part_counter, chapter_counter, line, "titlel2std")
      store_file_pair(part_counter, chapter_counter, line)
      # 匹配部标题，支持“第一部 标题”和“卷一 标题”两种格式
    elsif part_regex.match?(line)
      buffer = write_chapter_and_reset_buffer(buffer, part_counter, chapter_counter, output_dir)
      part_counter += 1
      chapter_counter = 0
      buffer << generate_html_structure(part_counter, chapter_counter, line, "titlel1single")
      store_file_pair(part_counter, chapter_counter, line)
      # 匹配章节标题
    elsif chapter_regex.match?(line)
      buffer = write_chapter_and_reset_buffer(buffer, part_counter, chapter_counter, output_dir)
      chapter_counter += 1
      buffer << generate_html_structure(part_counter, chapter_counter, line, "titlel2std")
      store_file_pair(part_counter, chapter_counter, line)
    else
      # 添加非空行的内容到缓冲区
      buffer << "<p class=\"a\">#{line}</p>\n"
    end
  end

  # 最后一次写入缓冲区内容
  write_to_html(buffer, part_counter, chapter_counter, output_dir) unless buffer.empty?
end

def write_chapter_and_reset_buffer(buffer, part_counter, chapter_counter, output_dir)
  unless buffer.empty?
    write_to_html(buffer, part_counter, chapter_counter, output_dir)
    buffer.clear
  end
  buffer
end

def store_file_pair(part_counter, chapter_counter, line)
  file_id = "chapter#{part_counter}-#{chapter_counter}"
  file_name = "#{file_id}.html"
  $file_pair << [file_id, file_name, line]
end

def generate_opf(title, author)
  content = ""
  content << generate_opf_structure(title, author)
  $file_pair.each do |id|
    content << "    <item id=\"#{id[0]}\" href=\"#{id[0]}.html\" media-type=\"application/xhtml+xml\"/>\n"
  end
  content << "  </manifest>"
  content << "\n"
  content << '  <spine toc="ncxtoc">'
  content << "\n"
  content << '    <itemref idref="cover" linear="no"/>'
  content << "\n"
  content << '    <itemref idref="htmltoc" linear="yes"/>'
  content << "\n"
  $file_pair.each do |id|
    content << "    <itemref idref=\"#{id[0]}\" linear=\"yes\"/>\n"
  end
  content << "  </spine>"
  content << "\n"
  content << "  <guide>"
  content << "\n"
  content << '    <reference type="cover" title="Cover" href="cover.html"/>'
  content << "\n"
  content << '    <reference type="toc" title="Table Of Contents" href="book-toc.html"/>'
  content << "\n"
  content << '    <reference type="text" title="Beginning" href="chapter0-0.html"/>'
  content << "\n"
  content << "  </guide>"
  content << "\n"
  content << "</package>"
  File.write("temp/OEBPS/content.opf", content)
end

def generate_opf_structure(title, author)
  <<~HTML
    <?xml version="1.0" encoding="utf-8"?>
    <package version="2.0" unique-identifier="bookid" xmlns="http://www.idpf.org/2007/opf">
      <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier id="bookid">easypub-1e2ee141</dc:identifier>
        <dc:title>#{title}</dc:title>
        <dc:creator opf:role="aut">#{author}</dc:creator>
        <dc:date>#{Time.now.iso8601}</dc:date>
        <dc:rights>Created with EasyPub v1.50</dc:rights>
        <dc:language>zh-CN</dc:language>
        <meta name="cover" content="cover-image"/>
        <meta property="ibooks:specified-fonts">true</meta>
      </metadata>
      <manifest>
        <item id="ncxtoc" href="toc.ncx" media-type="application/x-dtbncx+xml"/>
        <item id="htmltoc" href="book-toc.html" media-type="application/xhtml+xml"/>
        <item id="css" href="style.css" media-type="text/css"/>
        <item id="cover-image" href="cover.jpg" media-type="image/jpeg"/>
        <item id="cover" href="cover.html" media-type="application/xhtml+xml"/>
        <item id="embedded-font" href="font.ttf" media-type="application/x-truetype-font"/>
  HTML
end

def generate_cover(title)
  html_content = <<~HTML
    <?xml version="1.0" encoding="utf-8" ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-CN">
    <head>
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <meta name="generator" content="EasyPub v1.50" />
    <title>
    Cover
    </title>
    <style type="text/css">
    html,body {height:100%; margin:0; padding:0;}
    .wedge {float:left; height:50%; margin-bottom: -360px;}
    .container {clear:both; height:0; position: relative;}
    table, tr, th {height: 720px; width:100%; text-align:center;}
    </style>
    <link rel="stylesheet" href="style.css" type="text/css"/>
    </head>
    <body>
    <div class="wedge"></div>
    <div class="container">
    <table><tr><td>
    <img src="cover.jpg" alt="#{title}" />
    </td></tr></table>
    </div>
    </body>
    </html>
  HTML

  File.write("temp/OEBPS/cover.html", html_content)
end

def generate_ncx
  File.open("temp/OEBPS/toc.ncx", "w") do |file|
    prefix = <<~HTML
      <?xml version="1.0" encoding="utf-8" standalone="no"?>
      <!DOCTYPE ncx PUBLIC "-//NISO//DTD ncx 2005-1//EN" "http://www.daisy.org/z3986/2005/ncx-2005-1.dtd">
      <ncx xmlns="http://www.daisy.org/z3986/2005/ncx/" version="2005-1">
      <head>
      <meta name="cover" content="cover"/>
      <meta name="dtb:uid" content="easypub-1e2ee141" />
      <meta name="dtb:depth" content="2"/>
      <meta name="dtb:generator" content="EasyPub v1.50"/>
      <meta name="dtb:totalPageCount" content="0"/>
      <meta name="dtb:maxPageNumber" content="0"/>
      </head>
      
      <docTitle>
      <text>乱世书</text>
      </docTitle>
      <docAuthor>
      <text>姬叉</text>
      </docAuthor>
      
      <navMap>
      <navPoint id="cover" playOrder="1">
      <navLabel><text>封面</text></navLabel>
      <content src="cover.html"/>
      </navPoint>
      
      <navPoint id="htmltoc" playOrder="2">
      <navLabel><text>目录</text></navLabel>
      <content src="book-toc.html"/>
      </navPoint>
      
    HTML
    file.write(prefix)

    $file_pair.each do |i|
      content = <<~HTML
        <navPoint id="#{i[0]}" playOrder="4">
        <navLabel><text>#{i[2]}</text></navLabel>
        <content src="#{i[1]}"/>
        </navPoint>

      HTML
      file.write(content)
    end
    file.write("</navMap>\n</ncx>")
  end
end

def generate_book_toc
  prefix = <<~HTML
    <?xml version="1.0" encoding="utf-8" ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-CN">
    <head>
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <meta name="generator" content="EasyPub v1.50" />
    <title>
    Table Of Contents
    </title>
    <link rel="stylesheet" href="style.css" type="text/css"/>
    </head>
    <body>
    <h2 class="titletoc">
    目录
    </h2>
    <div class="toc">
    <dl>
  HTML

  File.open("temp/OEBPS/book-toc.html", "w") do |file|
    file.write(prefix)
    $file_pair.each do |i|
      file.write("<dt class=\"tocl#{i[0].end_with?("-0") && i[0] != "chapter0-0" ? 1 : 2}\"><a href=\"#{i[1]}\">#{i[2]}</a></dt>\n")
    end
    file.write("</dl>\n</div>\n</body>\n</html>")
  end
end

# 生成HTML骨架结构
def generate_html_structure(part_counter, chapter_counter, title, title_class)
  <<~HTML
    <?xml version="1.0" encoding="utf-8" ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-CN">
    <head>
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <meta name="generator" content="EasyPub v1.50" />
    <title>chapter #{part_counter} - #{chapter_counter}</title>
    <link rel="stylesheet" href="style.css" type="text/css"/>
    </head>
    <body>
    <h2 id="title" class="#{title_class}">#{title}</h2>
  HTML
end

# 将缓冲区内容写入到指定目录下的HTML文件
def write_to_html(content, part_counter, chapter_counter, output_dir)
  file_name = "chapter#{part_counter}-#{chapter_counter}.html"
  file_path = File.join(output_dir, file_name)
  File.open(file_path, "w") do |file|
    file.write(content)
    file.write("\n</body>\n</html>") # 添加HTML结束标签
  end
end

def copy_static_file(cover)
  FileUtils.cp("statics/font.ttf", "temp/OEBPS/font.ttf")
  FileUtils.cp("statics/style.css", "temp/OEBPS/style.css")
  if cover
    FileUtils.cp(cover, "temp/OEBPS/cover.jpg")
  end
end

options = {}
mandatory_options = [:title, :author, :txt]  # 定义必填选项

opt_parser = OptionParser.new do |opts|
  opts.banner = "Usage: script.rb --title TITLE --author AUTHOR --txt TXT --cover COVER"

  opts.on("--title TITLE", "Title of the book") do |title|
    options[:title] = title
  end

  opts.on("--author AUTHOR", "Author of the book") do |author|
    options[:author] = author
  end

  opts.on("--txt TXT", "Path to the txt file") do |txt|
    options[:txt] = txt
  end

  opts.on("--cover COVER", "Path to the cover image file") do |cover|
    if File.exist?(cover)
      options[:cover] = cover
    else
      puts "Cover file does not exist!"
      exit
    end
  end
end

opt_parser.parse!

# 检查必填选项是否都存在
missing_options = mandatory_options.select { |param| options[param].nil? }
unless missing_options.empty?
  puts "Missing options: #{missing_options.join(', ')}"
  puts opt_parser.banner
  exit
end

# Access the parsed options
puts "Title: #{options[:title]}"
puts "Author: #{options[:author]}"
puts "Txt: #{options[:txt]}"
puts "Cover: #{options[:cover]}"

# 使用实例
part_regex    = /^(第[一二三四五六七八九十]+[部卷]|卷[一二三四五六七八九十零]+)/
chapter_regex = /^第[1234567890一二三四五六七八九十百千零]+章/
prelog_regex  = /内容简介/
title         = options[:title]
author        = options[:author]
cover         = options[:cover]
txt           = options[:txt]

ensure_directories
create_mimetype_and_container_files
process_novel(txt, part_regex, chapter_regex, prelog_regex)
generate_opf(title, author)
generate_cover(title)
generate_ncx
generate_book_toc
copy_static_file(cover)

source_dir   = 'temp'
files_to_zip = %w[mimetype META-INF OEBPS]
output_file  = "#{title}.epub"

if File.exist?(output_file)
  FileUtils.rm(output_file)
end

# 创建 ZIP 文件
Zip::File.open(output_file, Zip::File::CREATE) do |zipfile|
  files_to_zip.each do |item|
    path = File.join(source_dir, item)
    if File.directory?(path)
      Dir[File.join(path, '**', '**')].each do |file|
        file_path_in_zip = file.sub(source_dir + '/', '')
        zipfile.add(file_path_in_zip, file)
      end
    else
      zipfile.add(item, path)
    end
  end
end

puts "生成文件：#{output_file}"